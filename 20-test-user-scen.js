import http from 'k6/http';
import { sleep } from 'k6';
import { check } from 'k6';
import { SharedArray } from 'k6/data';


let data = new SharedArray("userData", function() {
    return JSON.parse(open('creds.json'));
});

export let options = {
    scenarios: {
        createAccount: {
            executor: 'shared-iterations',
            vus: 1,
            iterations: data.length,
            maxDuration: '3m',
        },
        authenticate: {
            executor: 'shared-iterations',
            startTime: '1m',
            vus: 1,
            iterations: data.length,
            maxDuration: '20m',
        },
    },
};


export default function () {
    let user = data[__VU - 1];

    // Scénario 1: Création de compte
    if (__ITER < data.length) {
        let res = http.post('http://141.125.109.193/api/auth/register', JSON.stringify(user), { headers: { 'Content-Type': 'application/json' } });
        check(res, { 'status was 200': (r) => r.status == 200 });
    }

    // Scénario 2: Authentification
    else {
        let res = http.post('http://141.125.109.193/api/auth/login', JSON.stringify(user), { headers: { 'Content-Type': 'application/json' } });
        check(res, { 'status was 200': (r) => r.status == 200 });
    }

    sleep(1);
}
