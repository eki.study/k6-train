import { Trend } from 'k6/metrics';
import http from 'k6/http';

export const options = {
    vus: 10,
    duration: '20s',
  }

let waiting_time = new Trend('waiting_time');

export default function () {
    let res = http.get('http://141.125.109.193/api/test');

    waiting_time.add(res.timings.waiting);
}
