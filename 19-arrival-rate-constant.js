import http from 'k6/http';
import { sleep } from 'k6';
export const options = {
    scenarios: {
        CrocoArrivalConst: {
            executor: 'constant-arrival-rate',
            duration: '30s',
      
            rate: 20,
      
            timeUnit: '1s',
      
            preAllocatedVUs: 30,
            maxVUs: 50
          },
        },
      };

export default function () {
    let res = http.get('https://test-api.k6.io/public/crocodiles/');
      if (res.status !== 200) {
        throw new Error('API not work');
      }
    

  sleep(1);
}
