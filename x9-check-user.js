import http from 'k6/http';
import {check} from 'k6';

export let options = {
  thresholds: {
    http_req_failed: ['rate<=0.05'],
    checks: ['rate>=0.9']
  },
};
export function setup() {
  let res = http.post('http://141.125.109.193/api/auth/login', {
    name: 'ekirichydd',
    password: 'password1996',
    email: 'test65@gmail.com'
  });

  if (res.status !== 200) {
    throw new Error('Authentification échouée');
  }

  let authToken = res.json().access_token;

  return authToken;
}

export default function(authToken) {
  
  let res = http.post('http://141.125.109.193/api/auth/me', {
    headers: {
      Authorization: `Bearer ${authToken}`
    },
  });

  check(res, {
    'is status 200': (r) => r.status === 200,
    'response include correctly': (r)=> r.body.includes('ekirichydd')
  });
}
