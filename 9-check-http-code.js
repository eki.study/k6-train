import http from 'k6/http';
import {check} from 'k6';

/*export let options = {
    thresholds: {
      http_req_failed: ['rate<=0.05'],
      checks: ['rate>=0.9'],
      http_req_duration: ['p(95)<=100']
    },
  }; */

  export let options={
    vus: 10,
    duration: '20s'
  }

export default function() {
  
  let res = http.get('http://141.125.109.193/api/test');

  check(res, {
    'is status 200': (r) => r.status === 200,
    'response include correctly': (r)=> r.body.includes('correctly')
  });
}

