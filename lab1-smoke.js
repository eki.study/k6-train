import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  discardResponseBodies: true,
  scenarios: {
    upload: {
      executor: 'ramping-vus',
      stages: [
        { duration: '10s', target: 3 },
      ],
      gracefulRampDown: '0s',
    },
    constatnload: {
        startTime: '11s',
      executor: 'constant-vus',
      vus: 5,
      duration: '40s'
    },
    download: {
        startTime: '50s',
        executor: 'ramping-vus',
        startVUs: 3,
        stages: [
          { duration: '10s', target: 0 },
        ],
        gracefulRampDown: '0s',
      },
  },
};

export default function () {
  http.get('http://141.125.109.193/api/auth/login');
  sleep(0.5);
}
