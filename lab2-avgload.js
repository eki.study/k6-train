import http from 'k6/http';
import { sleep } from 'k6';
export const options = {
    scenarios: {
      CrocoRamp: {
        executor: 'ramping-vus',
        startVUs: 0,
        stages: [
          { duration: '1m', target: 200 },
          { duration: '3m', target: 200 },
          { duration: '1m', target: 0 },
        ],
        gracefulRampDown: '0s',
      },
    },
  };

export default function () {
    let res = http.get('https://test-api.k6.io/public/crocodiles/', {
        
      });
      if (res.status !== 200) {
        throw new Error('API not work');
      }
    

  sleep(0.5);
}
