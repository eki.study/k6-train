import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  discardResponseBodies: true,
  scenarios: {
    contacts: {
      executor: 'ramping-arrival-rate',
      startVUs: 0,
      timeUnit: '1m',
      stages: [
        { duration: '3m', target: 200 },
        { duration: '1m', target: 0 },
      ],
    },
  },
};

export default function () {
  http.get('https://test.k6.io/contacts.php');
  sleep(0.5);
}
