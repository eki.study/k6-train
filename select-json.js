import { sleep } from 'k6';
import http from 'k6/http';
import { check } from 'k6';

export const options = {
    vus: 10,
    duration:'5s'
}
let users = JSON.parse(open('./creds.json'));

export default function () {
    let user = users[Math.floor(Math.random() * users.length)];
    console.log(`user:  ${user.password} pwd:  ${user.password}`)

    let res = http.post('http://example.com/login', {
        username: user.username,
        password: user.password,
    });
    

    // Vérifier que la requête a réussi
    check(res, {
        'login successful': (r) => r.status === 200,
    });

    sleep(1);
}
