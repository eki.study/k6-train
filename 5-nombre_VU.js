import {sleep} from 'k6';

export function setup() {
    let usernames = ['user1', 'user2', 'user3'];
    let passwords = ['password1', 'password2', 'password3'];
    let rand_user = Math.floor(Math.random() * usernames.length);
    let rand_pwd = Math.floor(Math.random() * usernames.length);
    let username = usernames[rand_user];
    let password = passwords[rand_pwd];
    let data=[username, password]
    return data;
}

export const options= {
    duration: '10s',
    vus: 10
}

export default function (data) {
    console.log("je suis le VU n°: "+__VU+ "- je lance mon ITER: n° ${__ITER} avec l'utilisateur `+ data[0]+ ");
    sleep(1);
}