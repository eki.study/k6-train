import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
    vus: 10,
    duration: '20s',
  }
export default function () {
    console.log(`je suis la VU n°: ${__VU} - je lance mon ITER: n° ${__ITER} avec l'utilisateur`);
    sleep(1);
}
