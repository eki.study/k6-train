import {sleep} from 'k6';

export function setup() {
    let usernames = ['user1', 'user2', 'user3'];
    let passwords = ['password1', 'password2', 'password3'];
    let rand_user = Math.floor(Math.random() * usernames.length);
    let rand_pwd = Math.floor(Math.random() * usernames.length);
    let username = usernames[rand_user];
    let password = passwords[rand_pwd];
    let data=[username, password]
    return data;
}

export const options= {
    duration: '5s',
    vus: 5
}

export default function (data) {
    console.log(`je suis le VU n°: ${__VU} - je lance mon ITER: n° ${__ITER} avec l'utilisateur `+ data[0]+ " dont le pwd est "+ data[1]);
    sleep(1);
}

export function teardown(data) {
    console.log("user: "+ data[0]+" pwd: "+ data[1]);
    data=null;
    if (data==null) {
        console.log("données utilisateurs supprimées")
    }
    else{
        console.log(data);
    }
}